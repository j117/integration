package dev.local;

import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.server.subsystem.sftp.AbstractSftpEventListenerAdapter;
import org.apache.sshd.server.subsystem.sftp.FileHandle;
import org.apache.sshd.server.subsystem.sftp.Handle;

import java.io.IOException;

public class ExtendedSftpEventListener extends AbstractSftpEventListenerAdapter {

    @Override
    public void writing(ServerSession session, String remoteHandle, FileHandle localHandle, long offset, byte[] data,
                        int dataOffset, int dataLen) throws IOException {
        super.writing(session, remoteHandle, localHandle, offset, data, dataOffset, dataLen);
        System.out.println("Data streamed from sftp remote server and written to file: " + localHandle.getFile());
//		System.out.println(new String(data));


    }

    @Override
    public void close(ServerSession session, String remoteHandle, Handle localHandle) {
        System.out.println("Session closed");
        System.out.println("Local handle: " + localHandle.getFile());
    }

}
