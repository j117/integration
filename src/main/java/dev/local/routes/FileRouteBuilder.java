package dev.local.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileRouteBuilder extends RouteBuilder{
	
	@Value("/temp/cameltest/input")
	private String inboundDirectory;
	
	@Value("/temp/cameltest/output")
	private String outboundDirectory;
	
	@Override
	public void configure() throws Exception {
		from("file:"+inboundDirectory)
		.to("file:"+outboundDirectory);
		
	}

}
