package dev.local;

import dev.local.routes.FileRouteBuilder;
import org.apache.camel.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//import org.apache.ftpserver.FtpServer;
//import org.apache.ftpserver.ftplet.FtpException;

@SpringBootApplication
public class SpringCamelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCamelApplication.class, args);

		MinaSFtpServer.configure();

//		ApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("ftp-server.xml");
////		FtpServer ftpServer = (FtpServer) classPathXmlApplicationContext.getBean("server");
//		try {
//			ftpServer.start();
//		} catch (FtpException e1) {
//			e1.printStackTrace();
//		}
		
		
		Main main = new Main();
		main.addRouteBuilder(new FileRouteBuilder());
		try {
			main.run();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
